package array

import (
	"errors"
)

func ArrWalk[I, T any](arr []I, callback func(key int, value I) T) []T {
	r := make([]T, 0, len(arr))

	for i, v := range arr {
		r = append(r, callback(i, v))
	}

	return r
}

func ArrForEach[I any](arr []I, callback func(key int, value I)) {
	for i, v := range arr {
		callback(i, v)
	}
}

func ArrFilter[I any](arr []I, callback func(key int, value I) bool) []I {
	r := make([]I, 0, len(arr))

	for i, v := range arr {
		if callback(i, v) {
			r = append(r, v)
		}
	}

	return r
}

func ArrConcat[I any](arr ...[]I) []I {
	r := make([]I, 0, len(arr))

	for _, arr := range arr {
		for _, v := range arr {
			r = append(r, v)
		}
	}

	return r
}

func ArrUniq[I comparable](arr []I) []I {
	r := make([]I, 0, len(arr))
	uniqMap := make(map[I]bool, len(arr))

	for _, v := range arr {
		uniqMap[v] = true
	}

	for v := range uniqMap {
		r = append(r, v)
	}

	return r
}

func ArrFind[I any](arr []I, callback func(key int, value I) bool) (I, error) {
	for i, v := range arr {
		if callback(i, v) {
			return v, nil
		}
	}

	return *new(I), errors.New("element not found")
}

func ArrReverse[I any](arr []I) []I {
	for i, j := 0, len(arr)-1; i < j; i, j = i+1, j-1 {
		arr[i], arr[j] = arr[j], arr[i]
	}

	return arr
}
