package array_test

import (
	"testing"

	"gitlab.com/SergeySlonimsky/array"
)

func TestArrWalk(t *testing.T) {
	type input struct {
		value string
	}

	type output struct {
		index int
		value string
	}

	t.Run("string struct arr", func(t *testing.T) {
		arr := make([]input, 0, 3)
		arr = append(arr, input{value: "val1"})
		arr = append(arr, input{value: "val2"})
		arr = append(arr, input{value: "val3"})

		result := array.ArrWalk(arr, func(key int, value input) output {
			return output{
				index: key,
				value: value.value,
			}
		})

		if len(result) != len(arr) {
			t.Errorf("invalid length: exp %d, got %d", len(arr), len(result))
		}
	})
}

func TestArrFind(t *testing.T) {
	type input struct {
		value string
	}

	t.Run("string struct arr", func(t *testing.T) {
		arr := make([]input, 0, 3)
		arr = append(arr, input{value: "val1"})
		arr = append(arr, input{value: "val2"})
		arr = append(arr, input{value: "val3"})

		result, _ := array.ArrFind(arr, func(key int, value input) bool {
			return value.value == "val2"
		})

		if result.value != "val2" {
			t.Errorf("error: exp %s, got %s", "val2", result.value)
		}
	})
}
